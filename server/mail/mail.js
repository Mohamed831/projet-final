const express = require("express");
const bodyParser = require("body-parser");
const nodemailer = require("nodemailer");
const app = express();

// bodyParser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const sendMail = (req, res) => {
  let transporter = nodemailer.createTransport({
    service: "Gmail",
    secure: false, // true for 465, false for other ports
    auth: {
      user: "myapp.test121@gmail.com", // generated ethereal user
      pass: "mutgab11" // generated ethereal password
    },
    tls: {
      rejectUnauthorized: false
    }
  });
  const output = `
   <p>you have a new contact request</p>
   <h3>Contact Details</h3>

   <ul>
       <li>Name: ${req.body.name}</li>
       <li>Email: ${req.body.email}</li>
       <li>Phone: ${req.body.phone}</li>
   </ul>
   <h3>Message:</h3>
   <p>${req.body.message}</p>
   `;

  // send mail with defined transport object
  let mailOptions = {
    from: '"Nodemailer contact" <myapp.test121@gmail.com>', // sender address
    to: "zoolsudany6@gmail.com", // list of receivers
    subject: "Node contact request", // Subject line
    text: "Hello world?", // plain text body
    html: output // html body
  };
  //
  transporter.sendMail(mailOptions, (err, info) => {
    if (err) {
      console.log("IF ERRR");
      return console.log(err);
    }
    res.status(200).send(info);
  });
};
module.exports = sendMail;
