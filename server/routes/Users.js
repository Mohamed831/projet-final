const express = require("express");
const cors = require("cors");
const bcrypt = require("bcrypt");
const router = express.Router();
const stripe = require("stripe")("sk_test_sKeXZH2f9LaBBvO2airmE07k00vI2Qg8kU");
const jwt = require("jsonwebtoken");
const User = require("../models/User");
const sendMail = require("../mail/mail");
router.use(cors());
process.env.SECRET_KEY = "secret";

// update user
router.put("/update/:id", (req, res, next) => {
  console.log(req.params.id, req.body);
  User.update(
    {
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email
    },
    {
      where: {
        id: req.params.id //this will be your id that you want to update
      }
    }
  )
    .then(function(rowsUpdate, updatedUser) {
      console.log(rowsUpdate);
      console.log(updatedUser);
      res.json(updatedUser);
    })
    .catch(next);
});
// delete user by id
router.delete("/Delete/:id", (req, res) => {
  console.log(req.params.id);
  User.destroy({
    where: {
      id: req.params.id //this will be your id that you want to delete
    }
  }).then(
    itemDeleted => {
      // itemDeleted will return number of items deleted
      if (itemDeleted === 1) {
        res.send("Deleted successfully");
      }
    },
    function(err) {
      res.send(err);
    }
  );
});
// get user by id
router.get("/getUser/:id", (req, res) => {
  User.findOne({
    where: { id: req.params.id }
  }).then(users => {
    res.json(users);
  });
});
// get all users
router.get("/AllUsers", (req, res) => {
  User.findAll().then(users => {
    res.json(users);
  });
});
// POST

router.post("/register", (req, res) => {
  const today = new Date();
  const userData = {
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
    password: req.body.password,
    created: today
  };
  User.findOne({
    where: {
      email: req.body.email
    }
  })
    .then(user => {
      if (!user) {
        const hash = bcrypt.hashSync(userData.password, 10);
        userData.password = hash;
        User.create(userData)
          .then(user => {
            let token = jwt.sign(user.dataValues, process.env.SECRET_KEY, {
              expiresIn: 1440
            });
            res.json({
              success: true,
              msg: "User created successfully ",
              token: token
            });
          })
          .catch(err => {
            res.send(err);
          });
      } else {
        res.json({ error: "user already exist" });
      }
    })
    .catch(err => {
      res.send(err);
    });
});
router.post("/login", (req, res) => {
  User.findOne({
    where: {
      email: req.body.email
    }
  })
    .then(user => {
      if (bcrypt.compareSync(req.body.password, user.password)) {
        let token = jwt.sign(user.dataValues, process.env.SECRET_KEY, {
          expiresIn: 1440
        });
        res.json({
          success: true,
          token: token
        });
      } else {
        res.send("user not exist");
      }
    })
    .catch(err => {
      res.send("user not exist");
    });
});
// profile
router.get("/profile", (req, res) => {
  let decoded = jwt.verify(
    req.headers["authorization"],
    process.env.SECRET_KEY
  );
  User.findOne({
    where: {
      id: decoded.id
    }
  })
    .then(user => {
      if (user) {
        res.json(user);
      } else {
        res.send("User does not exist");
      }
    })
    .catch(err => {
      res.send(err);
    });
});
router.post("/sendMail", (req, res) => {
  sendMail(req, res, (err, data) => {
    if (err) {
      res.status(500).json({ message: "internal error" + err });
    } else {
      res.json("Email sent!!");
    }
  });
});

router.post("/pay", (req, res) => {
  console.log(req.body);
  const charge = stripe.charges.create(
    {
      amount: 2500,
      currency: "eur",
      source: req.body.token
    },
    (err, charge) => {
      if (err) {
        throw err;
      }
      res.json({
        success: true,
        message: "payment done"
      });
    }
  );
});
module.exports = router;
