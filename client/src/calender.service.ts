import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { getLocaleDateFormat } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CalenderService {
  constructor(private db: AngularFirestore) { }
  getData(): Observable<any[]> {
    return this.db.collection('events').valueChanges().pipe(
      map(events => events.map(event => {
        const data: any = event;
        data.start = data.start.toDate();
        return data;
      }))
    );
  }
}
