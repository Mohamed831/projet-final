import { map, catchError, tap } from 'rxjs/operators';
import { Observable, of, ObservableInput } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';


export interface UserDetails {
  id: number;
  // profile_pic: Blob;
  first_name: string;
  last_name: string;
  email: string;
  password: string;
  exp: number;
  iat: number;
}
interface TokenResponse {
  token: string;
}
export interface TokenPayload {
  id: number;
  // profile_pic: Blob;
  first_name: string;
  last_name: string;
  email: string;
  password: string;
}
@Injectable()
export class AuthService {
  // private ApiUrl = 'http://localhost:3000/users';
  private ApiUrl = 'users';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'my-auth-token',
      'Access-Control-Allow-Origin': '*'
    })
  };
  private token: string;
  handleError: (err: any, caught: Observable<any>) => ObservableInput<any>;
  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) { }
  private saveToken(token: string): void {
    localStorage.setItem('userToken', token);
    this.token = token;
  }
  private getToken(): string {
    if (!this.token) {
      this.token = localStorage.getItem('userToken');
    }
    return this.token;
  }
  public getUserDetails(): UserDetails {
    const token = this.getToken();
    let PayLoad;
    if (token) {
      PayLoad = token.split('.')[1];
      PayLoad = window.atob(PayLoad);
      return JSON.parse(PayLoad);
    } else {
      return null;
    }
  }

  public isLoggedIn(): boolean {
    const user = this.getUserDetails();
    if (user) {
      return user.exp > Date.now() / 1000;
    } else {
      return false;
    }
  }
  public register(user: TokenPayload): Observable<any> {
    const base = this.http.post(`${this.ApiUrl}/register`, user);

    const request = base.pipe(
      map((data: TokenResponse) => {
        if (data.token) {
          this.saveToken(data.token);
        }
        return data;
      })
    );
    return request;
  }
  public login(user: TokenPayload): Observable<any> {
    const base = this.http.post(`${this.ApiUrl}/login`, user);

    const request = base.pipe(
      map((data: TokenResponse) => {
        if (data.token) {
          this.saveToken(data.token);
        }
        return data;
      })
    );
    return request;
  }
  public getAll(): Observable<any> {
    return this.http.get(`${this.ApiUrl}/AllUsers`);
  }
  // DELETE: delete user from the server
  public removeUser(id: number): Observable<{}> {
    return this.http.delete(`${this.ApiUrl}/Delete/${id}`)
      .pipe(
        tap(selectedUser => console.log(`SelectedeUser = ${JSON.stringify(selectedUser)}`)),
        catchError(this.handleError)
      );
  }
  // get user by id
  public getUser(id: number): Observable<{}> {
    const url = `${this.ApiUrl}/getUser/${id}`;
    return this.http.get(url, this.httpOptions)
      .pipe(
        tap(selectedUser => console.log(`SelectedeUser = ${JSON.stringify(selectedUser)}`)),
        // catchError(this.handleError)
      );
  }
  // update user
  public editUser(id: any, user: any): Observable<{}> {
    // public editUser(user: UserDetails): Observable<{}> {
    // const id = user.id;
    console.log(user);
    const url = `${this.ApiUrl}/update/${id}`;
    return this.http.put(url, user, {
      headers: { authorization: `${this.getToken()}` }
    })
      // return this.http.post(url, user, this.httpOptions)
      .pipe(
        tap(updatedUser => console.log(`updated User = ${JSON.stringify(updatedUser)}`)),
        catchError(this.handleError)
      );
  }
  // get profile
  public profile(): Observable<any> {
    return this.http.get(`${this.ApiUrl}/profile`, {
      headers: { authorization: `${this.getToken()}` }
    });
  }
  public Logout(): void {
    this.token = '';
    window.localStorage.removeItem('userToken');
    this.router.navigateByUrl('/');
  }
  public sendMessage(messageContent: any) {
    return this.http.post(
      `/users/sendMail`,
      JSON.stringify(messageContent),
      {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
        responseType: 'text'
      }
    );
  }
}
