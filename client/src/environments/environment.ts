// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDE0fNUFP7Nw5wgOBcCQCL6VhiMd0fZCP0',
    authDomain: 'mycalender-f5c07.firebaseapp.com',
    databaseURL: 'https://mycalender-f5c07.firebaseio.com',
    projectId: 'mycalender-f5c07',
    storageBucket: 'mycalender-f5c07.appspot.com',
    messagingSenderId: '581558913821',
    appId: '1:581558913821:web:f26e09adf05f840098aff4',
    measurementId: 'G-CVF71FGE1R'
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
