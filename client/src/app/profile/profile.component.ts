import { AuthService, UserDetails } from 'src/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  details: UserDetails;
  constructor(
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.auth.profile().subscribe(
      user => {
        this.details = user;
      },
      err => {
        console.error(err);
      }
    );
  }
  delete(id: number) {
    if (confirm('Are you sure to delete this user ?!')) {
      this.auth.removeUser(id).subscribe(res => {
        console.log('user deleted successfully!');
      });
      this.auth.Logout();
    }
  }
  edit(id: number) {
    this.router.navigate(['/update', id]);
  }
}
