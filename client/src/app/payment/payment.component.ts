import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StripeService, Elements, Element as StripeElement, ElementsOptions } from 'ngx-stripe';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  elements: Elements;
  card: StripeElement;

  // optional parameters
  elementsOptions: ElementsOptions = {
    locale: 'es'
  };

  stripeTest: FormGroup;

  constructor(private fb: FormBuilder,
              private stripeService: StripeService,
              private http: HttpClient,
              private router: Router,
              public dialogRef: MatDialogRef<any>) { }

  ngOnInit() {
    this.stripeTest = this.fb.group({
      name: ['', [Validators.required]]
    });
    this.stripeService.elements(this.elementsOptions).subscribe(elements => {
      this.elements = elements;
      // Only mount the element the first time
      if (!this.card) {
        this.card = this.elements.create('card', {
          style: {
            base: {
              iconColor: '#666EE8',
              color: '#31325F',
              lineHeight: '30px',
              fontWeight: 300,
              fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
              fontSize: '0.9rem',
              '::placeholder': {
                color: '#CFD7E0'
              }
            }
          }
        });
        this.card.mount('#card-element');
      }
    });
  }
  closeDialog() {
    this.dialogRef.close();
  }

  buy() {
    const name = this.stripeTest.get('name').value;
    this.stripeService.createToken(this.card, { name }).subscribe(result => {
      if (result.token) {
        // Use the token to create a charge or a customer
        // https://stripe.com/docs/charges
        console.log(result.token);
        this.http
          .post('http://localhost:3000/users/pay', {
            token: result.token.id
          })
          .subscribe(res => {
            console.log('The response from server is : ', res);
            console.log('Paymen Is Done');
            this.closeDialog();
            this.router.navigate(['/success', name]);
          });
      } else if (result.error) {
        // Error creating the token
        console.log(result.error);
      }
    });
  }
}
