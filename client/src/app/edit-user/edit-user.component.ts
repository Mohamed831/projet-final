import { Component, OnInit, HostListener } from '@angular/core';
import { AuthService, TokenPayload, UserDetails } from 'src/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  userId: number;
  editForm: FormGroup;
  disabledSubmitButton = true;
  user: UserDetails;
  details: {
    id: 0;
    first_name: '';
    last_name: '';
    email: '';
    password: '';
  };
  @HostListener('input') oninput() {
    if (this.editForm.valid) {
      this.disabledSubmitButton = false;
    }
  }
  constructor(
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    console.log(
      this.route.params.subscribe(p => {
        console.log(p);
        this.details = p.id;
      })
    );
    this.editForm = new FormGroup({
      first_name: new FormControl(''),
      last_name: new FormControl(''),
      email: new FormControl('')
    });
  }
  onEdit(d) {
    console.log(d);
    console.log(this.user);

    const user2 = {
      id: d,
      first_name: this.editForm.get('first_name').value,
      last_name: this.editForm.get('last_name').value,
      email: this.editForm.get('email').value
    };

    this.auth.editUser(d, user2).subscribe(
      () => {
        this.router.navigateByUrl('/profile');
      },
      err => {
        console.error(err);
      }
    );
  }

  ngOnInit() {
    this.auth.profile().subscribe(
      user => {
        this.user = user;
      },
      err => {
        console.error(err);
      }
    );
  }
  goBack(): void {
    this.router.navigateByUrl('/profile');
  }
}
