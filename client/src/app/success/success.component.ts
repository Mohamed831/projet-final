import { ActivatedRoute } from '@angular/router';
import { StripeService } from 'ngx-stripe';
import { PaymentComponent } from './../payment/payment.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css']
})
export class SuccessComponent implements OnInit {
  name: '';
  constructor(private Service: StripeService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.params.subscribe(param => {
      this.name = param.name;
    });
  }
}
