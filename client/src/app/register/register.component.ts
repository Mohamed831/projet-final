import { AuthService, TokenPayload } from 'src/auth.service';
import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  inscriptionForm: FormGroup;
  disabledSubmitButton = true;
  submitted = false;
  user: TokenPayload;
  messageError = '';
  errorMsg = false;
  @HostListener('input') oninput() {
    if (this.inscriptionForm.valid) {
      this.disabledSubmitButton = false;
    }
  }

  constructor(
    private router: Router,
    private auth: AuthService,
    private fb: FormBuilder
  ) {}
  register() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.inscriptionForm.invalid) {
      return;
    } else {
      this.user = {
        id: 0,
        first_name: this.inscriptionForm.get('first_name').value,
        last_name: this.inscriptionForm.get('last_name').value,
        email: this.inscriptionForm.get('email').value,
        password: this.inscriptionForm.get('password').value
      };
      this.auth.register(this.user).subscribe(data => {
        if (data.success) {
          this.auth.login(this.user).subscribe(res => {
            console.log(res.success);
            if (res.success === true) {
              this.auth.profile();
              this.router.navigateByUrl('/profile');
            }
          });
        } else {
          this.errorMsg = true;
          this.messageError = data.msg;
        }
      });
    }
  }

  ngOnInit() {
    this.inscriptionForm = this.fb.group({
      first_name: [
        '',
        [Validators.required, Validators.minLength(3), Validators.maxLength(15)]
      ],
      last_name: [
        '',
        [Validators.required, Validators.minLength(3), Validators.maxLength(15)]
      ],
      email: ['', [Validators.required, Validators.email]],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(12),
          Validators.pattern('^(?=.*\\d).{8,12}$')
        ]
      ]
    });
  }
  get f() {
    return this.inscriptionForm.controls;
  }
}

// import { AuthService, TokenPayload } from 'src/auth.service';
// import { Component } from '@angular/core';
// import { Router } from '@angular/router';
// import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

// @Component({
//   selector: 'app-register',
//   templateUrl: './register.component.html',
//   styleUrls: ['./register.component.css']
// })
// export class RegisterComponent {

//   credentials: TokenPayload = {
//     id: 0,
//     first_name: '',
//     last_name: '',
//     email: '',
//     password: ''
//   };
//   constructor(private auth: AuthService, private router: Router) {}
//   register() {
//       this.auth.register(this.credentials).subscribe(
//       () => {
//         this.router.navigateByUrl('/profile');
//       },
//       err => {
//         console.error(err);
//       }
//     );
//     }

//   }
