import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService, TokenPayload } from 'src/auth.service';

@Component({
  selector: 'app-top-navbar',
  templateUrl: './top-navbar.component.html',
  styleUrls: ['./top-navbar.component.css']
})
export class TopNavbarComponent implements OnInit {
  isOpen = false;
  constructor(public auth: AuthService, private router: Router) {}

  ngOnInit() {}
  toggleNavbar() {
    this.isOpen = !this.isOpen;
  }
}
