import { Router, ActivatedRoute } from '@angular/router';
import { AuthService, TokenPayload } from 'src/auth.service';
import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  message: string;
  id: number;

  constructor(
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {}
  user: TokenPayload;
  users: any = [];

  ngOnInit() {
    this.auth
      .getAll()
      .pipe(first())
      .subscribe(users => {
        this.users = users;
      });
  }
}
