import { CalenderService } from './../../calender.service';
import { Component, OnInit } from '@angular/core';
// tslint:disable-next-line: import-spacing
import dayGridPlugin from '@fullcalendar/daygrid';

@Component({
  selector: 'app-calender',
  templateUrl: './calender.component.html',
  styleUrls: ['./calender.component.css']
})
export class CalenderComponent implements OnInit {
  calenderEvents: any[] = [];
  calenderPlugins = [dayGridPlugin];
  constructor(private service: CalenderService) {}

  ngOnInit() {
    this.service.getData().subscribe(data => (this.calenderEvents = data));
  }
}
